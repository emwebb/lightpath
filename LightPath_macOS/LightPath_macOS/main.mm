//
//  main.m
//  LightPath_macOS
//
//  Created by Ethan Webb on 28/11/2017.
//  Copyright © 2017 Ethan. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include "GameManager.h"
int main(int argc, const char * argv[]) {
    GameManager(argc,argv);
    return 0;
}
