//
//  GameManager.h
//  LightPath_macOS
//
//  Created by Ethan Webb on 29/11/2017.
//  Copyright © 2017 Ethan. All rights reserved.
//

#ifndef GameManager_h
#define GameManager_h

class GameManager {
    
    
public:
    GameManager(int argc, const char* argv[]);
};

#endif /* GameManager_h */
